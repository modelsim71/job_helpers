# _*_ coding: utf-8 _*_
import logging
import smtplib
from email.header import Header
from email.message import Message
from email.mime.text import MIMEText


class MailBox(object):
    def __init__(self, sender) -> None:
        self.sender = sender if sender else "network-qe@redhat.com"

    def __init__(
        self,
        smtp_server="smtp.corp.redhat.com",
        smtp_port=25,
        username="network-qe@redhat.com",
    ):
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.username = username

    def send_email(self, to_email=[], cc_mail=[], subject="", body=""):
        sender = self.username
        receivers = ",".join(set(to_email) | set(cc_mail))

        message = MIMEText(body, "plain", "utf-8")
        message["From"] = self.username
        message["To"] = ",".join(to_email)
        message["Cc"] = ",".join(cc_mail)
        message["Subject"] = Header(subject, "utf-8")

        with smtplib.SMTP(self.smtp_server, self.smtp_port) as server:
            server.sendmail(sender, receivers, message.as_string())


if __name__ == "__main__":
    mb = MailBox()
    to = ["xudu@redhat.com"]
    cc = ["xudu@redhat.com"]
    mb.send_email(to, cc, subject="test", body="This is a test.")
