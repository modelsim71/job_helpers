# _*_ coding: utf-8 _*_
import json
import re
import time
import traceback

import gspread

from utils import util

DEFAULT_PROPERTIES = {"orig": (1, 1), "range": (500, 26)}

INSERT_BEFORE = 1
INSERT_AFTER = 2


class Formatter(object):
    RED = {"red": 1.0, "green": 0.5, "blue": 0.5}
    BLUE = {"red": 0.0, "green": 0.0, "blue": 1.0}
    GREEN = {"red": 0.0, "green": 1.0, "blue": 0.0}
    ORANGE = {"red": 1.0, "green": 0.8, "blue": 0.5}
    GRAY = {"red": 0.8, "green": 0.8, "blue": 0.8}
    WHITE = {"red": 1.0, "green": 1.0, "blue": 1.0}
    BLACK = {"red": 0.0, "green": 0.0, "blue": 0.0}

    def __init__(self, format):
        self._format = format

    @property
    def format(self):
        if hasattr(self, self._format):
            return getattr(self, self._format)()
        else:
            return self._format

    @classmethod
    def fields(cls):
        return {
            "backgroundColor": cls.GRAY,
            "horizontalAlignment": "CENTER",
            "textFormat": {"fontSize": 10, "bold": True},
        }

    @classmethod
    def default(cls):
        return {
            "backgroundColor": cls.WHITE,
            "horizontalAlignment": "LEFT",
            "textFormat": {
                "foregroundColor": cls.BLACK,
                "fontSize": 10,
                "bold": False,
            },
        }

    @classmethod
    def error(cls):
        return {
            "backgroundColor": cls.RED,
        }

    @classmethod
    def warn(cls):
        return {
            "backgroundColor": cls.ORANGE,
        }


class GspreadSheet(object):
    def __init__(self, credit):
        self.scope = [
            "https://spreadsheets.google.com/feeds",
            "https://www.googleapis.com/auth/drive",
        ]
        self.credit = credit
        self._client = gspread.service_account_from_dict(credit)
        self.worksheets = {}

    def open_gsheet(self, name=None, id=None, owners=[]):
        try:
            if id:
                self._gspread = self._client.open_by_key(id)
            else:
                self._gspread = self._client.open(name)
        except gspread.exceptions.SpreadsheetNotFound as e:
            self._gspread = self._client.create(name)
            self.add_owners(owners)
        except Exception as e:
            traceback.print_stack()

        link = "https://docs.google.com/spreadsheets/d/" + str(self._gspread.id)
        print(f"gspread sheet link is {link}")

        return self._gspread

    def add_owners(self, owners):
        for owner in owners:
            self._gspread.share(owner["user"], owner["perm_type"], owner["role"])

    def open_worksheet(self, title, properties=DEFAULT_PROPERTIES):
        try:
            ws = self._gspread.worksheet(title)
        except gspread.exceptions.WorksheetNotFound as e:
            ws = self._gspread.add_worksheet(
                title, properties["range"][0], properties["range"][1]
            )
        except Exception as e:
            traceback.print_exc()

        self.worksheets[title] = properties
        return ws

    def delete_worksheet(self, title):
        try:
            ws = self._gspread.worksheet(title)
            self._gspread.del_worksheet(ws)
        except gspread.exceptions.WorksheetNotFound as e:
            pass
        except Exception as e:
            traceback.print_exc()
        finally:
            if title in self.worksheets:
                self.worksheets.pop(title)

    def clear_worksheet(self, title, row=0, col=0):
        ws = self._gspread.worksheet(title)
        ws.clear_basic_filter()
        if not row and not col:
            row, col = self.worksheets[title].get("orig")
        row_range, col_range = self.worksheets[title].get("range")
        range_start = gspread.utils.rowcol_to_a1(row, col)
        range_end = gspread.utils.rowcol_to_a1(row_range - 1, col_range - 1)
        ws.format(range_start + ":" + range_end, Formatter("default").format)
        ws.clear_notes([range_start + ":" + range_end])
        # only clear work area, don't use ws.clear()
        ws.batch_clear([range_start + ":" + range_end])

    def _batch_worksheet_data(self, ws, batch_data):
        sheet_data = []
        for data in batch_data:
            row, col = data["origin"]
            range_start = gspread.utils.rowcol_to_a1(row, col)
            range_end = gspread.utils.rowcol_to_a1(
                row + len(data["data"]) - 1, col + len(data["data"][0]) - 1
            )
            sheet_data.append(
                {"range": range_start + ":" + range_end, "values": data["data"]}
            )
        ws.batch_update(sheet_data)

    def _convert_record_to_data(self, orig, fields, record, pad=True):
        data = []

        if isinstance(record, dict):
            data = list(map(lambda field: record.get(field, ""), fields))
        elif isinstance(record, list):
            data.extend(record)
        else:
            data.append(record)

        return util.leftpad(data, orig[1] + len(data) - 1) if pad else data

    def _convert_data_to_record(self, orig, fields, data, pad=True):
        strip_data = util.strip_leftpad(data, orig[1] - 1) if pad else data
        if not fields:
            record = strip_data
        else:
            record = {
                f: strip_data[i] if i < len(strip_data) else ""
                for i, f in enumerate(fields)
            }
        return record

    def find_worksheet_values(self, title: str, search: str):
        ws = self._gspread.worksheet(title)
        criteria = re.compile(search)
        cells = ws.findall(criteria)
        return [cell.value for cell in cells]

    # only support to specify column match
    def find_worksheet_values_range(self, title: str, search):
        rows, cols = [], []
        ws = self._gspread.worksheet(title)
        fields = self.worksheets[title].get("fields", [])
        _, col = self.worksheets[title].get("orig")

        if isinstance(search, dict):
            search_key = [
                (search.get(f), (col + i)) for i, f in enumerate(fields) if f in search
            ]
        elif isinstance(search, list):
            search_key = [(f, (col + i)) for i, f in enumerate(search)]
        else:
            search_key = [(search, None)]

        matching_cells = [
            ws.findall(re.compile(key[0]), in_column=key[1]) for key in search_key
        ]

        if matching_cells:
            match_rows = {cell.row for cell in matching_cells[0]}
            for cells in matching_cells[1:]:
                match_rows &= {cell.row for cell in cells}

            rows = [] if not match_rows else list(match_rows)

            match_cols = {cell.row for cell in matching_cells[0]}
            for cells in matching_cells[1:]:
                match_rows &= {cell.row for cell in cells}

            cols = [] if not match_cols else list(match_cols)

        return rows, cols

    def refill_worksheet_all_records(self, title: str, records: list):
        self.clear_worksheet(title)

        # add tab fields
        orig = self.worksheets[title].get("orig")
        row, col = orig[0], orig[1]
        fields = self.worksheets[title].get("fields", [])

        if fields:
            self.insert_worksheet_row_records(title, [fields], row=row)
            self.format_worksheet_row_records(title, Formatter("fields"), rows=[row])
            row += 1

        data = [
            self._convert_record_to_data(orig, fields, record, pad=False)
            for record in records
        ]
        batch_data = [{"origin": (row, col), "data": data}]
        ws = self._gspread.worksheet(title)
        self._batch_worksheet_data(ws, batch_data)

    def get_worksheet_all_records(self, title: str) -> list:
        ws = self._gspread.worksheet(title)
        orig = self.worksheets[title].get("orig")
        range = self.worksheets[title].get("range")
        fields = self.worksheets[title].get("fields", [])

        range_start = gspread.utils.rowcol_to_a1(orig[0], orig[1])
        range_end = gspread.utils.rowcol_to_a1(range[0] - 1, range[1] - 1)
        all_values = ws.get_values(range_start + ":" + range_end)

        records = list(
            map(
                lambda value: self._convert_data_to_record(
                    orig, fields, value, pad=False
                ),
                all_values,
            )
        )
        if records and list(records[0].values()) == fields:
            records.pop(0)

        return records

    def insert_worksheet_row_records(
        self,
        title: str,
        records: list,
        row: int = 0,
        search=None,
        mode: int = INSERT_BEFORE,
    ):
        sheet_data = []

        ws = self._gspread.worksheet(title)
        orig = self.worksheets[title].get("orig")
        fields = self.worksheets[title].get("fields", [])
        pad = True if row or search else False
        sheet_data = [
            self._convert_record_to_data(orig, fields, record, pad)
            for record in records
        ]

        if row:
            ws.insert_rows(
                sheet_data,
                row=row if mode == INSERT_BEFORE else row + 1,
                value_input_option="RAW",
            )
        elif search:
            rows, _ = self.find_worksheet_values_range(title, search)
            if rows:
                ws.insert_rows(
                    sheet_data,
                    row=int(rows[0]) if mode == INSERT_BEFORE else int(rows[0]) + 1,
                    value_input_option="RAW",
                )
            else:
                ws.append_rows(
                    sheet_data,
                    value_input_option="RAW",
                    insert_data_option="INSERT_ROWS",
                )
        else:
            ws.append_rows(
                sheet_data, value_input_option="RAW", insert_data_option="INSERT_ROWS"
            )

    def remove_worksheet_row_records(self, title: str, rows: list = [], search=None):
        ws = self._gspread.worksheet(title)
        if not rows:
            rows, _ = self.find_worksheet_values_range(title, search)

        if rows:
            # [1, 2, 3, 7, 9, 10] -> [1-3, 7, 9-10]
            delete_rows = []
            rows.sort()
            end = start = rows[0]
            for i in range(1, len(rows)):
                if int(rows[i]) == int(end) + 1:
                    end = rows[i]
                else:
                    if start == end:
                        delete_rows.append([start])
                    else:
                        delete_rows.append([start, end])
                    end = start = rows[i]
            if start == rows[-1]:
                delete_rows.append([start])
            else:
                delete_rows.append([start, end])

            for i in range(len(delete_rows) - 1, -1, -1):
                if len(delete_rows[i]) == 2:
                    ws.delete_rows(int(delete_rows[i][0]), int(delete_rows[i][1]))
                else:
                    ws.delete_rows(int(delete_rows[i][0]))

    def get_worksheet_row_records(self, title: str, rows: list = [], search=None):
        records = []

        ws = self._gspread.worksheet(title)
        orig = self.worksheets[title].get("orig")
        fields = self.worksheets[title].get("fields", [])
        if not rows:
            rows, _ = self.find_worksheet_values_range(title, search)
        records = {
            row: self._convert_data_to_record(orig, fields, ws.row_values(row))
            for row in rows
        }
        return records

    def _batch_format_rows(self, ws, batch_format):
        formats = []
        for format in batch_format:
            formats.append(
                {
                    "range": format["range"],
                    "format": format["format"],
                }
            )
        if formats:
            ws.batch_format(formats)

    def batch_format_worksheet_row_records(self, title: str, batch_data: dict):
        formats = []
        ws = self._gspread.worksheet(title)
        _, col = self.worksheets[title].get("orig")

        for data in batch_data:
            records = self.get_worksheet_row_records(title, search=data["match"])
            type = data["type"]
            for row, values in records.items():
                start = gspread.utils.rowcol_to_a1(row, col)
                end = gspread.utils.rowcol_to_a1(row, col + len(values) - 1)

                formats.append(
                    {
                        "range": start + ":" + end,
                        "format": Formatter(type).format,
                    }
                )
        self._batch_format_rows(ws, formats)

    def format_worksheet_row_records(
        self, title: str, formatter: Formatter, rows: list
    ):
        formats = []

        ws = self._gspread.worksheet(title)
        _, col = self.worksheets[title].get("orig")

        records = self.get_worksheet_row_records(title, rows)
        for row, record in records.items():
            start = gspread.utils.rowcol_to_a1(row, col)
            end = gspread.utils.rowcol_to_a1(row, col + len(record) - 1)

            formats.append(
                {
                    "range": start + ":" + end,
                    "format": formatter.format,
                }
            )
        self._batch_format_rows(ws, formats)


def get_gspread_sheet():
    try:
        with open("./etc/credits.json", "r") as f:
            credits = json.load(f)
        gs = GspreadSheet(credits)
    except Exception as e:
        traceback.print_exc()

    return gs


gsheet = get_gspread_sheet()

if __name__ == "__main__":
    gsheet.open_gsheet("test")
    sheets = {
        "test1": {
            "fields": ["F1", "F2", "F3", "F4"],
            "orig": (10, 10),
            "range": (500, 26),
        },
        "test2": {
            "fields": ["F1", "F2", "F3", "F4"],
            "orig": (1, 1),
            "range": (500, 26),
        },
    }

    for sheet, property in sheets.items():
        time.sleep(60)
        gsheet.delete_worksheet(sheet)
        gsheet.open_worksheet(sheet, properties=property)

        # test1
        gsheet.insert_worksheet_row_records(
            sheet, [["a1", "a2", "a3", "a4"], ["b1", "b2", "b3", "b4"]], row=1
        )
        gsheet.insert_worksheet_row_records(
            sheet, [["c1", "c2", "c3", "c4"], ["d1", "d2", "d3", "d4"]]
        )
        gsheet.insert_worksheet_row_records(
            sheet, [["e1", "e2", "e3", "e4"]], row=3, mode=INSERT_AFTER
        )
        gsheet.insert_worksheet_row_records(
            sheet, [["f1", "f2", "f3", "f4"]], row=3, mode=INSERT_BEFORE
        )
        gsheet.insert_worksheet_row_records(sheet, [["g1", "g2", "g3", "g4"]])
        gsheet.insert_worksheet_row_records(
            sheet, [["f1", "f1", "f3", "f4"]], search={"F1": "f1"}
        )
        gsheet.insert_worksheet_row_records(
            sheet,
            [["h1", "h1", "h3", "h4"]],
            search={"F1": "f1", "F2": "f1"},
            mode=INSERT_AFTER,
        )
        print(gsheet.get_worksheet_all_records(sheet))
        if sheet == "test1":
            gsheet.clear_worksheet("test1", 1, 1)

        # test2
        gsheet.remove_worksheet_row_records(sheet, rows=[1, 2])
        print(gsheet.get_worksheet_row_records(sheet, rows=[1, 2]))
        gsheet.remove_worksheet_row_records(sheet, search={"F1": "f1"})
        print(gsheet.get_worksheet_row_records(sheet, search={"F2": "g2"}))
        print(gsheet.get_worksheet_row_records(sheet, search={"F2": "g3"}))

        # test3
        gsheet.refill_worksheet_all_records(
            sheet,
            [
                ["a1", "ca2", "a3", "a4"],
                ["b1", "b2", "b3", "b4"],
                ["c1", "c2", "c3", "c4"],
            ],
        )
        print(gsheet.get_worksheet_all_records(sheet))
        gsheet.batch_format_worksheet_row_records(
            sheet, [{"match": {"F1": "a1"}, "type": "error"}]
        )
