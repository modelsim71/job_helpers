# _*_ coding: utf-8 _*_
(
    SUCCESS,
    BEAKER_ERROR,
    CONFIG_ERROR,
    INTERNAL_ERROR,
) = range(4)

JOBS_BATCH_SIZE = 100
