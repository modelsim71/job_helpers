# _*_ coding: utf-8 _*_
import json
import logging
import subprocess
from concurrent.futures import ThreadPoolExecutor

from common.constants import *
from common.exceptions import BeakerException
from common.jobparser import JobParser
from utils.beaker import BeakerProxy
from utils.util import import_cls

LOG = logging.getLogger(__name__)


class JobManager(object):
    def __init__(self, name):
        try:
            bkr_proxy = BeakerProxy()
            bkr_proxy.who_am_i()
        except Exception as e:
            raise BeakerException()

        cls = import_cls("common.jobparser" + "." + name + "JobParser")
        self.parser = cls() if callable(cls) else JobParser()

    def select_job_parser(self, parser):
        self.parser = parser

    def list_jobs(self, **kwargs):
        command = "bkr job-list"
        command += " -o " + kwargs["owner"] if kwargs.get("owner", None) else ""
        command += (
            " -w " + kwargs["whiteboard"] if kwargs.get("whiteboard", None) else ""
        )
        command += (
            " -l " + str(kwargs["limit"]) if kwargs.get("limit", -1) != -1 else ""
        )
        command += (
            " --min-id " + kwargs["minid"].split(":")[1]
            if kwargs.get("minid", None)
            else ""
        )
        command += (
            " --max-id " + kwargs["maxid"].split(":")[1]
            if kwargs.get("maxid", None)
            else ""
        )
        command += " --finished " if kwargs.get("finished", None) else ""
        command += " --unfinished " if kwargs.get("unfinished", None) else ""
        LOG.debug(command)

        with subprocess.Popen(
            command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            output = proc.stdout.read()
            jobs = json.loads(bytes.decode(output, encoding="utf8"))

        return jobs

    def get_jobs_result(self, jobs: list) -> list:
        results = []

        for job in jobs:
            command = f"bkr job-results --prettyxml {job} > /dev/shm/{job}.xml"
            with subprocess.Popen(
                command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
            ) as proc:
                proc.stdout.read().decode(encoding="utf8")

            self.parser.load_job_xml_file(f"/dev/shm/{job}.xml")
            results.extend(self.parser.parse_job_data())

        return results

    def batch_jobs_result(self, **kwargs) -> list:
        jobs = self.list_jobs(**kwargs)

        start, end, chunks = 0, 0, []

        size = len(jobs) // JOBS_BATCH_SIZE
        remainder = len(jobs) % JOBS_BATCH_SIZE

        for i in range(JOBS_BATCH_SIZE):
            end = start + size + (1 if i < remainder else 0)
            chunks.append(jobs[start:end])
            if end >= len(jobs):
                break
            start = end

        with ThreadPoolExecutor() as executor:
            results = list(executor.map(self.get_jobs_result, chunks))

        job_results = []
        for i, result in enumerate(results, 1):
            job_results.extend(result)

        if not kwargs.get("duplicate"):
            job_results = self.remove_duplicates(job_results)

        return job_results

    def remove_duplicates(self, job_data):
        return self.parser.eliminate_duplicates(job_data)


if __name__ == "__main__":
    mgr = JobManager("eeprom")
    print(mgr.list_jobs(whiteboard="eeprom-9.4.0-ctc1-round2"))
    print(mgr.list_jobs(whiteboard="eeprom-9.4.0-ctc1-round2", unfinished=True))
    print(
        mgr.list_jobs(
            whiteboard="eeprom-9.4.0-ctc1-round2", minid="J:8605174", maxid="J:8605174"
        )
    )
    print(mgr.batch_jobs_result(whiteboard="eeprom-9.4.0-ctc1-round2", limit=2))
    print(len(mgr.batch_jobs_result(whiteboard="eeprom-9.4.0-ctc1-round2", limit=100)))

    mgr = JobManager("leapp")
    print(
        mgr.batch_jobs_result(
            whiteboard="leapp-810-ctc1", minid="J:8710758", maxid="J:8710758"
        )
    )
