# _*_ coding: utf-8 _*_
import os

import toml

from common.exceptions import ParamException


def load_config():
    config = {}
    etc_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__))) + "/etc"
    for conf_name in ["config"]:
        url = os.path.join(etc_path, "%s.toml" % conf_name)
        config[conf_name] = toml.loads(open(url).read())
    return config["config"]


def get_config(prj, ver):
    try:
        conf = {}
        for k, v in g_config.items():
            if prj not in g_config[k]:
                conf[k] = v
            else:
                conf[k] = {}
                for k1, v1 in g_config[k][prj].items():
                    if ver != k1 and k1 not in conf[k]:
                        conf[k][k1] = v1
                    else:
                        conf[k].update(g_config[k][prj][ver])
        return conf
    except Exception as e:
        raise ParamException()


g_config = load_config()

if __name__ == "__main__":
    print(get_config("leapp", "leapp810to94"))
