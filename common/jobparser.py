# _*_ coding: utf-8 _*_
import logging
import re
import time
from itertools import groupby

import requests
from lxml import etree as ET

LOG = logging.getLogger(__name__)


def wrap_retry(max_times=3, seconds=1):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            logging.getLogger("urllib3").setLevel(logging.WARNING)
            for cnt in range(max_times):
                try:
                    return func(self, *args, **kwargs)
                except Exception as e:
                    if cnt < (max_times - 1):
                        LOG.warning(f"Operation retry {cnt} {e}")
                        time.sleep(seconds)
                    else:
                        LOG.error(e)
                        return None

        return wrapper

    return decorator


class JobParser(object):
    def __init__(self):
        pass

    @wrap_retry(10)
    def _get(self, url):
        requests.packages.urllib3.disable_warnings()
        response = requests.get(url, verify=False)
        return response.text

    def _parse_result_data(self, root, parse_logs=True):
        data = {}
        path = root.get("path")
        if path == "Setup" or path == "Cleanup":
            return None
        else:
            data.setdefault("result_path", path)
        data.setdefault("result_result", root.get("result"))
        if parse_logs:
            data.setdefault("result_logs", [])
            for log in root.getiterator("log"):
                link = log.get("href")
                data["result_logs"].append(link)
        return data

    def _parse_task_data(self, root, pares_results=True):
        data = {}
        task_name = root.get("name")
        if task_name.find("/kernel/networking") == -1:
            return None
        else:
            data.setdefault("task_name", task_name)
            data.setdefault("task_result", root.get("result"))
            data.setdefault("task_status", root.get("status"))

        for param in root.getiterator("param"):
            param_name = param.get("name")
            param_value = param.get("value")
            data.setdefault(param_name, param_value)

        task_logs = root.find(".//logs[1]")
        data.setdefault("task_logs", [])
        for log in task_logs.getiterator("log"):
            task_log_link = log.get("href")
            task_log_name = log.get("name")
            if task_log_name in ["harness.log", "journal.xml", "taskout.log"]:
                continue
            data["task_logs"].append(task_log_link)

        if pares_results:
            data.setdefault("task_results", [])
            for result in root.getiterator("result"):
                result_data = self._parse_result_data(result)
                if result_data:
                    data["task_results"].append(result_data)
        return data

    def _parse_recipe_data(self, root, parse_tasks=True):
        data = {}
        data.setdefault("recipe_id", root.get("id"))

        hostname_elements = root.findall(".//hostname")
        for element in hostname_elements:
            data.setdefault("recipe_hostname", element.get("value"))

        if parse_tasks:
            data.setdefault("recipe_tasks", [])
            for task in root.getiterator("task"):
                task_data = self._parse_task_data(task)
                if task_data:
                    data["recipe_tasks"].append(task_data)
        return data

    def _pasre_job_data(self, root, pasre_recipes=True):
        root = self.tree.getroot()
        data = {}
        for job in root.getiterator("job"):
            data.setdefault("job_status", job.get("status"))
        job_id = job.get("id")
        data.setdefault("job_id", "J:" + job_id)
        data.setdefault("job_owner", job.get("owner").replace("@redhat.com", ""))
        data.setdefault(
            "job_link", "https://beaker.engineering.redhat.com/jobs/" + str(job_id)
        )
        data.update(data)

        if pasre_recipes:
            data.setdefault("job_recipes", [])
            for recipe in root.getiterator("recipe"):
                recipe_data = self._parse_recipe_data(recipe)
                data["job_recipes"].append(recipe_data)

        return data

    def load_job_xml_file(self, xml_file):
        self.xml_file = xml_file
        self.tree = ET.parse(xml_file)

    def parse_job_data(self):
        root = self.tree.getroot()
        data = self._pasre_job_data(root)
        LOG.info(data)
        return data

    def _eliminate(self, data, key_func, keep_func):
        seen = {}

        for item in data:
            key = key_func(item)

            if key not in seen or keep_func(item, seen[key]):
                seen[key] = item

        return list(seen.values())


class EepromJobParser(JobParser):
    def __init__(self):
        super().__init__()

    def extract_log_data(self, log_url):
        data = {}

        content = self._get(log_url)
        pattern_line = {
            "PCI": (r"bus-info: (\S+)", 1),
            "Driver": (r"driver: (\S+)", 1),
            "VendorName": (r"Vendor name\s*:\s*(\S+)", 1),
            "VendorOUI": (r"Vendor OUI\s*:\s*(\S+)", 1),
            "VendorPN": (r"Vendor PN\s*:\s*(\S+)", 1),
            "VendorRev": (r"Vendor rev\s*:\s*(\S+)", 1),
            "Compliance": (r".*(cmplnce|Compliance).*:\s*(.*)", 2),
            "TransceiverCodes": (r"Transceiver codes\s*:\s*(.*)", 1),
        }
        for k, v in pattern_line.items():
            match = re.search(v[0], content)
            data[k] = match.group(v[1]) if match else "N/A"

        pattern_lines = {
            "TransceiverType": r"^\s*Transceiver type\s*:\s*(.*)$",
        }
        for k, v in pattern_lines.items():
            pattern = re.compile(v, re.MULTILINE)
            matches = pattern.findall(content)
            matched_lines = []
            for match in matches:
                matched_lines.append(match)
            data[k] = "\n".join(matched_lines) if matched_lines else "N/A"

        return data

    def extract_eeprom_data(self, job_data):
        eeprom_data = []

        job_id = job_data["job_id"]
        job_status = job_data["job_status"]
        for recipe in job_data["job_recipes"]:
            hostname = recipe["recipe_hostname"]
            for task in recipe["recipe_tasks"]:
                task_name = task["task_name"]
                if "sanity" not in task_name:
                    continue
                if job_status == "Completed":
                    for result in task["task_results"]:
                        result_result = result["result_result"]
                        path = result["result_path"]
                        if "eeprom-extended-info" not in path:
                            continue
                        nic_name = path.split("-")[-1]
                        eeprom = {
                            "Machine": hostname,
                            "JobId": job_id,
                            "Result": result_result,
                            "NIC": nic_name,
                        }

                        for log_url in result["result_logs"]:
                            if "resultoutputfile" in log_url:
                                log_data = self.extract_log_data(log_url)
                                break

                        eeprom.update(log_data)
                        eeprom_data.append(eeprom)
                elif job_status != "Cancelled":
                    eeprom = {
                        "Machine": hostname,
                        "JobId": job_id,
                        "Result": job_status,
                    }
                    eeprom_data.append(eeprom)

        return eeprom_data

    def parse_job_data(self) -> list:
        data = super().parse_job_data()
        return self.extract_eeprom_data(data)

    def eliminate_duplicates(self, data):
        def determine(g):
            if all(d["Result"] == "Pass" for d in g):
                return "Pass"
            elif any(d["Result"] == "Aborted" for d in g):
                return "Aborted"
            else:
                return "Any"

        def keep(g1, g2):
            prio = {i: status for i, status in enumerate(["Aborted", "Any", "Pass"])}
            p1 = prio.get(determine(g1))
            p2 = prio.get(determine(g2))
            return p1 > p2

        seen = {}
        sorted_data = sorted(data, key=lambda x: x["JobId"])
        for _, g in groupby(sorted_data, key=lambda x: x["JobId"]):
            group = list(g)
            key = group[0]["Machine"]
            if key not in seen or keep(group, seen[key]):
                seen[key] = group

        return [value for valus in seen.values() for value in valus]


class LeappJobParser(JobParser):
    def __init__(self):
        super().__init__()

    def parse_job_data(self) -> list:
        data = super().parse_job_data()
        return self.extract_leapp_data(data)

    def extract_leapp_data(self, job_data):
        leapp_data = []

        job_id = job_data["job_id"]
        job_status = job_data["job_status"]
        for recipe in job_data["job_recipes"]:
            data = {}
            machine = recipe["recipe_hostname"]

            if job_status == "Completed":
                leapp_task_result = "Pass"
                for task in recipe["recipe_tasks"]:
                    task_name = task["task_name"]
                    if "update_via_leapp" in task_name:
                        leapp_task_result = task["task_result"]
                    elif "nic_driver_info" in task_name:
                        urls = [log for log in task["task_logs"] if "_nic" in log]

                nic_content = self._get(urls[-1])
                data = {"Machine": machine, "JobId": job_id, "Note": nic_content}
                data.update(self.extract_nic_info(nic_content))

                if leapp_task_result != "Pass":
                    data["Result"] = "UpgradeFail"
                elif data["Result"] == "":
                    data["Result"] = "Pass"

            elif job_status != "Cancelled":
                data = {
                    "Machine": machine,
                    "JobId": job_id,
                    "Result": job_status,
                }

            if data:
                leapp_data.append(data)

        return leapp_data

    def extract_nic_info(self, nicinfo):
        fields = [
            "Result",
            "Driver",
            "VFDriver",
            "NICNoSupp",
            "NICNewSupp",
            "NICNoConsist",
            "VFNoConsist",
        ]
        data = {f: set() for f in fields}

        rows = nicinfo.strip().split("\n")[2:]
        for row in rows:
            columns = [col.strip() for col in row.strip("|").split("|")]

            driver = columns[1]
            model = columns[2]
            ifacename_o = columns[4]
            mac_o = columns[5]
            ifacename_t = columns[6]
            mac_t = columns[7] if len(columns) > 7 else mac_o
            is_vf = "_VF" in driver

            if not is_vf:
                data["Driver"].add(driver)

                if ifacename_t == "N/A":
                    data["NICNoSupp"].add(driver + "  " + model)
                    data["Result"].add("NICNoSupp")
                elif ifacename_o == "":
                    data["NICNewSupp"].add(driver + "  " + model)
                    data["Result"].add("NICNewSupp")
                elif ifacename_o != ifacename_t or mac_o != mac_t:
                    data["NICNoConsist"].add(driver + "  " + model)
                    data["Result"].add("NICNoConsist")
            else:
                data["VFDriver"].add(driver[:-3])
                if ifacename_o != ifacename_t or mac_o != mac_t:
                    data["VFNoConsist"].add(driver[:-3])

        return {key: "\n".join(map(str, value)) for key, value in data.items()}

    def eliminate_duplicates(self, data):
        def key(d):
            return d["Machine"]

        def keep(d1, d2):
            prio = {
                i: status
                for i, status in enumerate(["Aborted", "Any", "Queued", "Pass"])
            }

            p1 = prio.get(d1["Result"], "Any")
            p2 = prio.get(d2["Result"], "Any")

            return p1 > p2

        return self._eliminate(data, key_func=key, keep_func=keep)


if __name__ == "__main__":
    parser = JobParser()
    parser.load_job_xml_file("/dev/shm/J:8605292.xml")
    print(parser.parse_job_data())
    parser = EepromJobParser()
    parser.load_job_xml_file("/dev/shm/J:8605113.xml")
    print(parser.parse_job_data())
    parser = EepromJobParser()
    parser.load_job_xml_file("/dev/shm/J:8605292.xml")
    print(parser.parse_job_data())
    parser = EepromJobParser()
    parser.load_job_xml_file("/dev/shm/J:8605174.xml")
    print(parser.parse_job_data())
    parser.load_job_xml_file("/dev/shm/J:8605143.xml")
    print(parser.parse_job_data())
