# _*_ coding: utf-8 _*_
import argparse
import logging
import sys
import traceback

from common.config import get_config
from common.jobmanager import JobManager
from common.logger import setup_logging
from utils.gsheet import Formatter, gsheet

LOG = logging.getLogger(__name__)


def collect_jobs(conf):
    try:
        parser = conf["job"]["parser"]
        jobmgr = JobManager(parser)

        owner = conf["job"]["owner"]
        wb = conf["job"]["whiteboard"]
        duplicate = conf["job"].get("duplicate", True)
        results = jobmgr.batch_jobs_result(
            owner=owner, whiteboard=wb, duplicate=duplicate
        )

        project = conf["sheet"]["project"]
        owners = conf["sheet"]["owners"]
        gsheet.open_gsheet(
            project,
            owners=owners,
        )

        title = conf["sheet"]["title"]
        properties = {
            "orig": tuple(conf["sheet"]["orig"]),
            "range": tuple(conf["sheet"]["range"]),
            "fields": conf["sheet"]["fields"],
        }
        gsheet.open_worksheet(title=title, properties=properties)

        gsheet.refill_worksheet_all_records(title, results)
        formts = conf["sheet"]["format"]
        gsheet.batch_format_worksheet_row_records(
            title,
            formts,
        )
    except Exception as e:
        traceback.print_exc()


def monitor_jobs():
    pass


def usage():
    print(
        """
Usage: bin/environ.sh exec python3 cmds/helpers.py -n [project] -v [version] --collect

-- project     The project name assigned to the job, used to identify job execution, data analysis, and result recording.
-- version     Identifying different versions of the same job, executing tests on the different kernel.
-- collect     Collecting job execution results, analyzing data, and recording outcomes.  
-- monitor     Monitoring job runs, real-time data analysis, and result updates.

The script assists in managing job tasks, tracks job executions, analyzes job data, and records job results..

You must setup two files with tokens and parameters:

./etc/credits.joson

The script uses a gspread authentication token file for Google Sheets authentication, utilizing the default "network-driver" project. 
It creates different sheets within this shared project. See the address below.

https://console.cloud.google.com/iam-admin/serviceaccounts/details/112076677080414733823/keys?project=report-379205

./etc/common.toml

Configuration file that defines attributes, permissions, and other parameters for the job and google sheet in use.

Example: bin/environ.sh exec python3 cmds/helpers.py -p eeprom -v rhel94 --collect

	"""
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--project", help="job project")
    parser.add_argument("-v", "--version", help="job version")
    parser.add_argument("--collect", action="store_true", help="collect job result")
    parser.add_argument("--monitor", action="store_true", help="monitor job result")

    args = parser.parse_args()
    if args.project is None or args.version is None:
        usage()
        sys.exit(1)

    conf = get_config(args.project, args.version)
    setup_logging(conf["log"]["path"])

    if args.collect:
        collect_jobs(conf)
    elif args.watch:
        monitor_jobs()

    sys.exit(0)
