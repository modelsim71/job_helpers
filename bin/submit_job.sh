#!/bin/bash

#parameters
TOPO=alone

opts=()
sopts=()
copts=()
common_params=(
"--arch=x86_64"
"--retention-tag=active+1"
"--product=cpe:/o:redhat:enterprise_linux"
"--ks-meta=redhat_ca_cert no_networks"
"--param=SET_DMESG_CHECK_KEY=yes"
"--param=NAY=yes"
"--k-opts=selinux=0 enforcing=0"
"--k-opts-post=selinux=0 enforcing=0"
)
extra_params=()

#functions
match_system()
{
    local opts=("${@}")
	raw=$(./parse_netqe_nic_info.sh --raw "${opts[@]}")
	awk -v t="${opts[*]}" '{
    	split(t, opts, " ")
		f1 = $3
		for (i = 1; i <= length(opts); i++) {
			if (opts[i] ~ "--driver") {
				f2 = $4
			}
			if (opts[i] == "--model") {
				f3 = $6
			}
			if (opts[i] == "--speed") {
				f4 = $7
			}
		}
		f5 = $12

		print f1 "@" f2 "@" f3 "@" f4 "@" f5
	}' <<< "$raw" | sort -u
}

Usage()
{
	cat <<- END
	Tools for submitting job

	Input options:
		-D, --distro        Base distro
		-B, --brew          Brew kernel
		-M, --match         Match system
		-U, --unmatch       No match system
		-d, --driver        NIC driver
		-m, --model         NIC model
		-s, --speed         NIC speed
		--wb                White board
		--url               Fetch url
		--path              Task path

	Examples:
	# test module eeprom extend info
    ./submit_job.sh -D RHEL-8.10.0-20231121.1 --wb eeprom --match \.\*driver-Prototype \\
	--path nic/functions/ethtool/sanity --unmatch wsfd-netdev -- --param=RUN_FUNC=eeprom_extended_info

	# nic consistent via leapp upgrade
    ./submit_job.sh -D RHEL-8.10.0-20231214.0 --wb leapp-810-ctc1 --match \.\*driver-Prototype \\
	--path nic/functions/nic_driver_info,nic/functions/update_via_leapp,nic/functions/nic_driver_info \\
	-- --param=ORIGIN_VERSION=8.10 --param=TARGET_VERSION=9.4
	END
}

progname=${0##*/}

_at=$(getopt -o D:B:M:U:d:m:s:h \
	--long distro: \
	--long brew: \
	--long match: \
	--long unmatch: \
	--long driver: \
	--long model: \
	--long speed: \
	--long wb: \
	--long url: \
	--long path: \
	--long help \
	-n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
	-D|--distro)       distro="$2"; shift 2;;
	-B|--brew)         extra_params+=("--brew=$2"); shift 2;;
	--wb)              extra_params+=("--wb-comment=$2"); shift 2;;
	--url)             fetch_url="$2"; shift 2;;
	--path)            path="$2"; shift 2;;
	--match)           opts+=("--match $2"); shift 2;;
	--unmatch)         opts+=("--unmatch $2"); shift 2;;
	--driver)          opts+=("--driver $2"); shift 2;;
	--model)           opts+=("--model $2"); shift 2;;
	--speed)           opts+=("--speed $2"); shift 2;;
	-h|--help)         Usage; shift 1; exit 0;;
	--)                shift; break;;
	esac
done


if [[ -z $distro ]]; then
	echo "ERROR: distro must be exist!!!"
	exit 1
else
	MAJOR_VER=$(echo "$distro" | awk -F"-" '{print $2}' |  awk -F"." '{print $1}')
	# MINOR_VER=$(echo "$distro" | awk -F"-" '{print $2}' |  awk -F"." '{print $2}')
	if [ "$MAJOR_VER" -le 7 ];then
		extra_params+=("--variant=Server")
	else
		extra_params+=("--variant=baseos")
	fi

	if [ "$MAJOR_VER" -ge 9 ]; then
		extra_params+=("--cmd-and-reboot=update-crypto-policies --set LEGACY")
	fi
fi

if [[ -z $fetch_url ]]; then
	extra_params+=("--fetch-url=kernel,git://gerrit-git.engineering.redhat.com/kernel-tests")
else
	extra_params+=("--fetch-url=$fetch_url")
fi

if [[ -z $path ]]; then
	echo "ERROR: path must be exist!!!"
	exit 1
else
	rm -f tasks.list && touch tasks.list

	currdir=$(pwd)
	basedir="${currdir%/*/*}"
	IFS=',' read -ra subdirs <<< "$path"
	for subdir in "${subdirs[@]}"; do
		pushd "$basedir/$subdir" > /dev/null 2>&1 || exit
		lstest -t 4h >> "$currdir"/tasks.list
		popd > /dev/null 2>&1 || exit
	done

	if echo "$path" | grep -qv "update_via_leapp" ; then
		extra_params+=("--kdump=netqe-bj.usersys.redhat.com:/home/kdump/vmcore")
	fi
fi

if [[ ${#opts[@]} -eq 0 ]]; then
	echo "ERROR: match/driver/model/speed must be exist!!!"
	exit 1
else
	for opt in "${opts[@]}"; do
		key=${opt%% *}
		value=${opt##* }

		if [[ $value == *";"* ]]; then
			sopts+=("$key $([[ -z "${value%%;*}" ]] && echo "any" || echo "${value%%;*}")")
			copts+=("$key $([[ -z "${value##*;}" ]] && echo "any" || echo "${value##*;}")")
			TOPO=mh
		else
			sopts+=("$key" "$value")
			copts+=("$key" any)
		fi
	done
fi

if [[ $TOPO == alone ]]; then
    nic_info=$(match_system "${sopts[@]}")

	for info in $nic_info; do
		hostname=$(echo "$info" | awk -F '@' '{print $1}')
		systype=$(echo "$info" | awk -F '@' '{print $5}' | awk -F '-' '{print $2}')

		driver=$(echo "$info" | awk -F '@' '{print $2}')
		if [[ -n $driver ]]; then
			extra_params+=("--param=NIC_DRIVER=$driver ")
		fi

		model=$(echo "$info" | awk -F '@' '{print $3}')
		if [[ -n $model ]]; then
			extra_params+=("--param=NIC_MODEL=$model")
		fi

		speed=$(echo "$info" | awk -F '@' '{print $4}' | sed 's/[^0-9]//g')
		if [[ -n $speed ]]; then
			extra_params+=("--param=NIC_SPEED=$speed")
		fi

		runtest "$distro" --machine="$hostname" --systype="$systype" "${common_params[@]}" "${extra_params[@]}" "$@" < tasks.list
	done

else
	echo "multi-host server" "${sopts[@]}"
	echo "multi-host client" "${copts[@]}"
fi

rm tasks.list
