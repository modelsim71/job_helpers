#!/bin/sh
export LANG=en_US.utf8
SCRIPT_DIR=$(dirname "$0")

BASE_PATH=$(dirname "$(cd "$SCRIPT_DIR" && pwd -P)")

PROJECT_NAME="job_helpers"

LOG_DIR="/data/log/${PROJECT_NAME}"

PYTHONE_BIN="python3.8"

VIRTUALENV_NAME=".venv"
VIRTUALENV_PATH="$BASE_PATH/$VIRTUALENV_NAME"
#VIRTUALENV_BIN="/usr/bin/virtualenv"
VIRTUALENV_BIN="$PYTHONE_BIN -m venv"

PACKAGE_PATH="$BASE_PATH/.packages"

PIP_BIN="$VIRTUALENV_PATH/bin/pip3 --disable-pip-version-check --no-cache-dir"

REQUIREMENTS_LOCK="$BASE_PATH/requirements.lock"

create_venv() {
    if ! command -v $PYTHONE_BIN > /dev/null; then
        echo "$PYTHONE_BIN not found. Installing..."
        sudo yum install -y $PYTHONE_BIN "$(echo $PYTHONE_BIN | tr -d '.')"-devel
    fi

    if [ ! -d "$VIRTUALENV_PATH" ]; then
        #cd "$BASE_PATH" && $VIRTUALENV_BIN $VIRTUALENV_NAME --python=$PYTHONE_BIN --no-download
        cd "$BASE_PATH" && $VIRTUALENV_BIN $VIRTUALENV_NAME
    fi
}

destroy_venv() {
    if [ -d "$VIRTUALENV_PATH" ]; then
        if [ -n "${VIRTUALENV_PATH%/*}" ] && [ "${VIRTUALENV_PATH%/*}" != "/" ]; then
            cd "${VIRTUALENV_PATH%/*}" && rm -fr ${VIRTUALENV_NAME}
        fi
    fi
}

update_or_create_venv() {
    create_venv

    if [ -e "$PACKAGE_PATH" ]; then
        FILENAMES="$(ls "$PACKAGE_PATH")"
        INSTALLED_PACKAGES="$(${PIP_BIN} list --format freeze | sed 's/[=_-]//g')"
        NEW_PACKAGES=$(for FILENAME in $FILENAMES; do
            MATCH_KEY=$(echo "$FILENAME" | sed 's/[=_-]//g; s/\.tar.*//g; s/\.zip//g; s/\.whl//g')
            if ! echo "$INSTALLED_PACKAGES" | grep -i "$MATCH_KEY" > /dev/null; then
                echo "$PACKAGE_PATH/$FILENAME"
            fi
        done)
    elif [ -f "$REQUIREMENTS_LOCK" ]; then
        LOCK_PACKAGES=$(cat "$REQUIREMENTS_LOCK")
        INSTALLED_PACKAGES="$(${PIP_BIN} list --format freeze)"
        NEW_PACKAGES=$(for LOCK_PACKAGE in $LOCK_PACKAGES; do
            if ! echo "$INSTALLED_PACKAGES" | grep -i "$LOCK_PACKAGE"; then
                echo "$LOCK_PACKAGE"
            fi
        done)
    fi

    if [ -n "$NEW_PACKAGES" ]; then
        echo "installing $NEW_PACKAGES"
        if [ -e "$PACKAGE_PATH" ]; then
            $PIP_BIN install \
                --find-links file://"$PACKAGE_PATH" \
                --no-index \
                "$NEW_PACKAGES"
        else
            for NEW_PACKAGE in $NEW_PACKAGES; do
                $PIP_BIN install "$NEW_PACKAGE"
            done
        fi
        echo "done"
    fi
}

recreate_venv() {
    destroy_venv 
    update_or_create_venv
}

lockonly_packages() {
    $PIP_BIN freeze > "$REQUIREMENTS_LOCK"
}

lock_packages() {
    if $PIP_BIN check; then
        if [ -d "$PACKAGE_PATH" ]; then
            if [ -n "${PACKAGE_PATH%/*}" ] && [ "${PACKAGE_PATH%/*}" != "/" ]; then
                cd "${PACKAGE_PATH%/*}" && rm -fr "${PACKAGE_PATH##*/}"
            fi
        fi

        $PIP_BIN freeze > "$REQUIREMENTS_LOCK"

        $PIP_BIN download \
            --no-deps \
            -r "$REQUIREMENTS_LOCK" \
            -d "$PACKAGE_PATH"
    fi
}

unlock_packages() {
    if [ -d "$PACKAGE_PATH" ]; then
        if [ -n "${PACKAGE_PATH%/*}" ] && [ "${PACKAGE_PATH%/*}" != "/" ]; then
            cd "${PACKAGE_PATH%/*}" && rm -fr "${PACKAGE_PATH##*/}"
        fi
    fi
    cd "$BASE_PATH" && rm -rf "$REQUIREMENTS_LOCK"
}

cleanup() {
    if [ -f $LOG_DIR ] || [ -z "$(ls -A $LOG_DIR)" ]; then
        find $LOG_DIR -type f -mtime +30 -exec rm {} \;
    fi
}


case $1 in
    create|destroy|recreate|update_or_create)
        "$1"_venv
        ;;
    exec)
        export PATH="$VIRTUALENV_PATH/bin:$PATH"
        export PYTHONPATH="$BASE_PATH:$PYTHONPATH"
        "$@"
        ;;
    install|uninstall|check|list)
        ${PIP_BIN} "$@"
        ;;
    lock|lockonly|unlock)
        "$1"_packages
        ;;
    cleanup)
        $1
        ;;
    *)
        echo "VirtualEnv:"
        echo "  $0 create                   Create virtualenv"
        echo "  $0 destroy                  Destroy virtualenv"
        echo "  $0 recreate                 Recreate virtualenv"
        echo "  $0 update_or_create         Update or create virtualenv"
        echo "  $0 exec                     Excecute virtualenv commands"
        echo
        echo "Packages:"
        echo "  $0 install [packages]       Install packages"
        echo "  $0 uninstall [packages]     Uninstall packages"
        echo "  $0 list                     List installed packages"
        echo "  $0 lock                     Generate requirements.lock and download packages"
        echo
        echo "Logs:"
        echo "  $0 clean                    Clean logs"
        echo
        exit 1
        ;;
esac

